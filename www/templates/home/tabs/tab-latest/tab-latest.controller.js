angular
    .module('app')
    .controller('LatestCtrl', LatestCtrl);

function LatestCtrl($scope, $rootScope, LatestService, $state, $cordovaSocialSharing, $ionicLoading) {
    $scope.page = 1;
    $scope.moredata = true;
    LatestService.getPosts($scope.page)
        .then(function(data) {
            $scope.posts = data;
            $rootScope.rootLoader = false;
        })
        .catch(function(error) {
            $rootScope.rootLoader = false;
            $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
        });

    $scope.goSingle = function(post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: false});
    };

    $scope.goSingleComments = function (post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: true});
    };

    $scope.doRefresh = function () {
        $scope.page = 1;
        LatestService.getPosts($scope.page)
            .then(function(data) {
                $scope.posts = data;
                $scope.moredata = true;
                $scope.$broadcast('scroll.refreshComplete');
            })
            .catch(function(error) {
                console.log(error);
                $scope.$broadcast('scroll.refreshComplete');
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
    };

    $scope.loadMore = function () {
        $scope.moredata = false;
        $scope.page++;
        LatestService.getPosts($scope.page)
            .then(function(data) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                angular.forEach(data, function(post) {
                    $scope.posts.push(post);
                });
                console.log($scope.posts);
                $scope.moredata = true;
            })
            .catch(function(error) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                console.log(error);
                $scope.moredata = false;
            });
        $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    $scope.fbShare = function(link) {
        $cordovaSocialSharing
            .shareViaFacebook(null, null, link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Facebook aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.twitterShare = function(link) {
        $cordovaSocialSharing
            .shareViaTwitter(null, null, link)
            .then(function(result) {
                console.log("Then", result);
            })
            .catch(function(err) {
                console.log("Error", err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Twitter aplikacija', noBackdrop: true, duration: 2000 });
            });
    };
}