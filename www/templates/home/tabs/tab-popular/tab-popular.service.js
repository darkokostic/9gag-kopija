angular
    .module('app')
    .service('PopularService', PopularService);

function PopularService($q, $http) {
    function getPosts(page, perPage) {
        var deferred = $q.defer();
        var params = {
            page: page,
            per_page: perPage
        };
        $http.get('http://dev2.cdm.me/wp-json/api/posts/popular?_embed', {params: params})
            .success(function (data) {
                angular.forEach(data, function(post) {
                    var commParams = {
                        post: post.id,
                        per_page: 1,
                        page: 1
                    };
                    $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: commParams})
                        .success(function (comments, status, headers) {
                            post.points = 0;
                            post.comments = [];
                            post.comments_count = headers('X-WP-Total');
                        })
                        .error(function (error) {
                            deferred.reject(error);
                        });
                });
                deferred.resolve(data);
            })
            .error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    return {
        getPosts: getPosts
    };
}