angular
    .module('app')
    .controller('TrendingCtrl', TrendingCtrl);

function TrendingCtrl($scope, $rootScope, TrendingService, $state, $ionicLoading, $cordovaSocialSharing) {
    $scope.page = 1;
    $scope.perPage = 5;
    $scope.moredata = true;
    TrendingService.getPosts($scope.page, $scope.perPage)
        .then(function(data) {
            console.log(data);
            $scope.posts = data;
            if($scope.posts == null) {
                $scope.moredata = false;
            }
            $rootScope.rootLoader = false;
        })
        .catch(function(error) {
            $scope.moredata = false;
            $rootScope.rootLoader = false;
            $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
        });

    $scope.goSingle = function(post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: false});
    };

    $scope.goSingleComments = function (post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: true});
    };

    $scope.doRefresh = function () {
        $scope.page = 1;
        TrendingService.getPosts($scope.page, $scope.perPage)
            .then(function(data) {
                $scope.posts = data;
                $scope.moredata = true;
                $scope.$broadcast('scroll.refreshComplete');
            })
            .catch(function(error) {
                console.log(error);
                $scope.$broadcast('scroll.refreshComplete');
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
    };

    $scope.loadMore = function () {
        $scope.moredata = false;
        $scope.page++;
        TrendingService.getPosts($scope.page, $scope.perPage)
            .then(function(data) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                angular.forEach(data, function(post) {
                    $scope.posts.push(post);
                });
                console.log($scope.posts);
                $scope.moredata = data != null;
            })
            .catch(function(error) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                console.log(error);
                $scope.moredata = false;
            });
        $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    $scope.fbShare = function(link) {
        $cordovaSocialSharing
            .shareViaFacebook(null, null, link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Facebook aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.twitterShare = function(link) {
        $cordovaSocialSharing
            .shareViaTwitter(null, null, link)
            .then(function(result) {
                console.log("Then", result);
            })
            .catch(function(err) {
                console.log("Error", err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Twitter aplikacija', noBackdrop: true, duration: 2000 });
            });
    };
}