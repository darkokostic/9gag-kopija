angular
    .module('app')
    .service('SinglePostService', SinglePostService);

function SinglePostService($q, $http, $localStorage) {
    function getTags(tags) {
        var deferred = $q.defer();
        var newTags = [];
        angular.forEach(tags, function (tag) {
            $http.get('http://dev2.cdm.me/wp-json/wp/v2/tags/' + tag)
                .success(function (data, status, headers) {
                    console.log(data);
                    newTags.push(data);
                })
                .error(function (error) {
                    deferred.reject(error);
                });
        });
        deferred.resolve(newTags);

        return deferred.promise;
    }

    function loadMoreReplies(page, comment) {
        var deferred = $q.defer();
        var params = {
            parent: comment.id,
            page: page,
            per_page: 5
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: params})
            .success(function (replies, status, headers) {
                angular.forEach(replies, function(reply) {
                    comment.hasMoreReplies = replies.length > 4;
                    comment.replyPage = page;
                    comment.replies.push(reply);
                });
            })
            .error(function (error) {
                deferred.reject(error);
            });
        deferred.resolve(comment);
        return deferred.promise;
    }

    function loadMoreComments(page, post) {
        var deferred = $q.defer();
        var params = {
            parent: 0,
            post: post.id,
            per_page: 10,
            page: page
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: params})
            .success(function (comments) {
                console.log(comments);
                angular.forEach(comments, function(comment) {
                    var params = {
                        parent: comment.id,
                        page: 1,
                        per_page: 5
                    };
                    $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: params})
                        .success(function (replies, status, headers) {
                            comment.hasMoreReplies = replies.length > 4;
                            comment.replyPage = 1;
                            comment.replies = replies;
                        })
                        .error(function (error) {
                            deferred.reject(error);
                        });
                });

                deferred.resolve(comments);

            })
            .error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    function getFirstComments(post) {
        var deferred = $q.defer();
        var params = {
            parent: 0,
            post: post.id,
            per_page: 10,
            page: 1
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: params})
            .success(function (comments) {
                console.log(comments);
                angular.forEach(comments, function(comment) {
                    var params = {
                        parent: comment.id,
                        page: 1,
                        per_page: 5
                    };
                    $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: params})
                        .success(function (replies, status, headers) {
                            comment.hasMoreReplies = replies.length > 4;
                            comment.replyPage = 1;
                            comment.replies = replies;
                        })
                        .error(function (error) {
                            deferred.reject(error);
                        });
                });

                deferred.resolve(comments);

            })
            .error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    function postComment(parent, post, comment) {
        var deferred = $q.defer();
        $http.post('http://dev2.cdm.me/wp-json/wp/v2/comments', {post: post.id, parent: parent, author_name: $localStorage.user.display_name, author_email: $localStorage.user.user_email, content: comment})
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    return {
        getTags: getTags,
        loadMoreReplies: loadMoreReplies,
        loadMoreComments: loadMoreComments,
        getFirstComments: getFirstComments,
        postComment: postComment
    };
}