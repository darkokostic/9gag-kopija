angular
    .module('app')
    .controller('SinglePostCtrl', SinglePostCtrl);

function SinglePostCtrl($scope, $stateParams, $rootScope, AuthService, $ionicHistory, $location, $ionicLoading, $ionicScrollDelegate, $timeout, SinglePostService, $cordovaSocialSharing, $ionicPopup, IonicClosePopupService, $localStorage) {
    $scope.post = $stateParams.post;
    $scope.page = 1;
    $rootScope.newComment = {
        content: ''
    };
    if($scope.post.tags.length) {
        SinglePostService.getTags($scope.post.tags)
            .then(function(data) {
                console.log(data);
                $scope.post.tags = data;
            })
            .catch(function(error) {
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
    }
    SinglePostService.getFirstComments($scope.post)
        .then(function(data) {
            $rootScope.rootLoader = false;
            $scope.post.comments = data;
            $scope.post.hasMoreComments = data.length > 9;
        })
        .catch(function(error) {
            $rootScope.rootLoader = false;
            $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
        });

    $scope.replay = false;
    $scope.replayInReplay = false;

    $scope.goComments = function() {
        if($stateParams.comments) {
            $timeout(function(){
                $location.hash('comments');
                $ionicScrollDelegate.$getByHandle('scroll').anchorScroll("#" + $location.hash('comments'));
            },500);
        }
    };

    $scope.goComments();

    $scope.goBack = function() {
        $ionicHistory.goBack();
    };

    $scope.makeReplyAvailable = function (comment) {
        for(var i = 0; i < $scope.post.comments.length; i++) {
            if(comment.id == $scope.post.comments[i].id) {
                $scope.post.comments[i].isNewReply = true;
            }
        }
    };

    $scope.makeReplyUnavailable = function (comment) {
        for(var i = 0; i < $scope.post.comments.length; i++) {
            if(comment.id == $scope.post.comments[i].id) {
                $scope.post.comments[i].isNewReply = false;
            }
        }
    };

    $scope.loadMoreComments = function() {
        $scope.page++;
        SinglePostService.loadMoreComments($scope.page, $scope.post)
            .then(function(data) {
                angular.forEach(data, function(comment) {
                    $scope.post.comments.push(comment);
                });
                $scope.post.hasMoreComments = data.length > 9;
            })
            .catch(function(error) {
                console.log(error);
            });
    };

    $scope.loadMoreReplies = function(comment) {
        comment.replyPage++;
        SinglePostService.loadMoreReplies(comment.replyPage, comment)
            .then(function(data) {
                for(var i = 0; i < $scope.post.comments.length; i++) {
                    if($scope.post.comments[i].id == comment.id) {
                        $scope.post.comments[i] = data;
                    }
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    };

    $scope.fbShare = function() {
        $cordovaSocialSharing
            .shareViaFacebook(null, null, $scope.post.link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Facebook aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.twitterShare = function() {
        $cordovaSocialSharing
            .shareViaTwitter(null, null, $scope.post.link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Twitter aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.postComment = function(parentId, comment) {
        SinglePostService.postComment(parentId, $scope.post, comment.content)
            .then(function(data) {
                $rootScope.newComment.content = '';
                if(data.status != "approved") {
                    $ionicLoading.show({ template: "Vaš komentar čeka odobrenje administratora", noBackdrop: true, duration: 2000 });
                } else {
                    if(data.parent == 0) {
                        $scope.post.comments.unshift(data);
                    } else {
                        for(var i = 0; i < $scope.post.comments.length; i++) {
                            if(data.parent == $scope.post.comments[i].id) {
                                $scope.post.comments[i].replies.unshift(data);
                                $scope.post.comments[i].newReply.content = '';
                                $scope.makeReplyUnavailable($scope.post.comments[i]);
                            }
                        }
                    }
                    $ionicLoading.show({ template: "Uspešno ste postavili komentar", noBackdrop: true, duration: 2000 });
                }
            })
            .catch(function(error) {
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
                $rootScope.newComment.content = '';
            });
    };

    $scope.openAuthPopup = function(parentId, comment) {
        $rootScope.credentials = {
            username: null,
            password: null
        };
        $rootScope.newComment.content = comment.content;
        $rootScope.parentId = parentId;
        $scope.authPopup = $ionicPopup.show({
            templateUrl: "./templates/auth-popup/auth-popup.html",
            scope: $scope
        });
        IonicClosePopupService.register($scope.authPopup);
    };

    $scope.login = function(credentials) {
        if(credentials.username && credentials.password) {
            AuthService.login(credentials)
                .then(function(data) {
                    $localStorage.token = data.token;
                    $localStorage.user = data.user;
                    SinglePostService.postComment($rootScope.parentId, $scope.post, $rootScope.newComment.content)
                        .then(function(data) {
                            $scope.authPopup.close();
                            $rootScope.newComment.content = '';
                            if(data.status != "approved") {
                                $ionicLoading.show({ template: "Vaš komentar čeka odobrenje administratora", noBackdrop: true, duration: 2000 });
                            } else {
                                if(data.parent == 0) {
                                    $scope.post.comments.unshift(data);
                                } else {
                                    for(var i = 0; i < $scope.post.comments.length; i++) {
                                        if(data.parent == $scope.post.comments[i].id) {
                                            $scope.post.comments[i].replies.unshift(data);
                                            $scope.post.comments[i].newReply.content = '';
                                            $scope.makeReplyUnavailable($scope.post.comments[i]);
                                        }
                                    }
                                }
                                $ionicLoading.show({ template: "Uspešno ste postavili komentar", noBackdrop: true, duration: 2000 });
                            }
                        })
                        .catch(function(error) {
                            $scope.authPopup.close();
                            $rootScope.newComment.content = '';
                            $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
                        });
                })
                .catch(function(error) {
                    $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
                });
        } else {
            $ionicLoading.show({ template: "Unesite korisničko ime i šifru", noBackdrop: true, duration: 2000 });
        }
    };

    $scope.goSingleCommentsScroll = function () {
        $location.hash('postcomment');
        $ionicScrollDelegate.$getByHandle('scroll').anchorScroll("#" + $location.hash('postcomment'));
    };
}