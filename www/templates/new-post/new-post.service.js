angular
    .module('app')
    .service('NewPostService', NewPostService);

function NewPostService($q, $http) {
    function createPost(post) {
        var deferred = $q.defer();
        console.log(post);
        $http.post('http://dev2.cdm.me/wp-json/wp/v2/posts', {author: post.author, title: post.title, content: post.content, category: post.category.id, excerpt: post.content, status: "publish"})
            .success(function (data) {
                console.log(data);
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });

        return deferred.promise;
    }

    return {
        createPost: createPost
    }
}