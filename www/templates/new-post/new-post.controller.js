angular
    .module('app')
    .controller('NewPostCtrl', NewPostCtrl);

function NewPostCtrl($scope, $rootScope, $ionicPopup, IonicClosePopupService, $localStorage, NewPostService, $ionicLoading) {
    $rootScope.rootLoader = false;
    $scope.post = [];
    $scope.canAddVideo = false;
    $scope.categories = $localStorage.categories;
    $scope.categoryPlaceholder = "Izaberite kategoriju";
    $scope.choosedCategory = null;

    $scope.chooseCategory = function() {
        $scope.categoryPopup = $ionicPopup.show({
            templateUrl: "./templates/choose-category-popup/choose-category-popup.html",
            scope: $scope
        });
        IonicClosePopupService.register($scope.categoryPopup);
    };

    $scope.choose = function(category) {
        $scope.choosedCategory = category;
        $scope.categoryPlaceholder = category.name;
        $scope.categoryPopup.close();
    };

    $scope.create = function() {
        $scope.post.author = parseInt($localStorage.user.ID);
        $scope.post.category = $scope.choosedCategory;
        NewPostService.createPost($scope.post)
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
    };
}