angular
    .module('app')
    .service('SearchService', SearchService);

function SearchService($q, $http) {
    function getPosts(page, query) {
        var deferred = $q.defer();
        var params = {
            page: page,
            per_page: 5,
            search: query
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/posts?_embed', {params: params})
            .success(function (data) {
                angular.forEach(data, function(post) {
                    var commParams = {
                        post: post.id,
                        per_page: 1,
                        page: 1
                    };
                    $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: commParams})
                        .success(function (comments, status, headers) {
                            post.points = 0;
                            post.comments = [];
                            post.comments_count = headers('X-WP-Total');
                        })
                        .error(function (error) {
                            deferred.reject(error);
                        });
                });
                deferred.resolve(data);
            })
            .error(function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    }

    return {
        getPosts: getPosts
    };
}