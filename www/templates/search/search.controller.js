angular
    .module('app')
    .controller('SearchCtrl', SearchCtrl);

function SearchCtrl($scope, $rootScope, $stateParams, $state, $cordovaSocialSharing, $ionicLoading, SearchService) {
    $rootScope.searchPosts = $stateParams.data;
    $rootScope.query = $stateParams.query;
    $scope.page = 1;
    $scope.moredata = true;
    $rootScope.rootLoader = false;

    $scope.goSingle = function(post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: false});
    };

    $scope.goSingleComments = function (post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: true});
    };

    $scope.doRefresh = function () {
        $scope.page = 1;
        SearchService.getPosts($scope.page, $rootScope.query)
            .then(function(data) {
                $rootScope.searchPosts = data;
                $scope.moredata = true;
                $scope.$broadcast('scroll.refreshComplete');
            })
            .catch(function(error) {
                console.log(error);
                $scope.$broadcast('scroll.refreshComplete');
            });
    };

    $scope.loadMore = function () {
        $scope.moredata = false;
        $scope.page++;
        SearchService.getPosts($scope.page, $rootScope.query)
            .then(function(data) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                angular.forEach(data, function(post) {
                    $rootScope.searchPosts.push(post);
                });
                $scope.moredata = true;
            })
            .catch(function(error) {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                console.log(error);
                $scope.moredata = false;
            });
        $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    $scope.fbShare = function(link) {
        $cordovaSocialSharing
            .shareViaFacebook(null, null, link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Facebook aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.twitterShare = function(link) {
        $cordovaSocialSharing
            .shareViaTwitter(null, null, link)
            .then(function(result) {
                console.log("Then", result);
            })
            .catch(function(err) {
                console.log("Error", err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Twitter aplikacija', noBackdrop: true, duration: 2000 });
            });
    };
}