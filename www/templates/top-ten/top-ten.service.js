angular
    .module('app')
    .service('TopTenService', TopTenService);

function TopTenService($q, $http) {
    var self = {
        'page': 1,
        'isLoading': false,
        'hasMore': true,
        'results': [],
        'refresh': function () {
            self.page = 1;
            self.isLoading = false;
            self.hasMore = true;
            self.results = [];
            return self.load();
        },
        'next': function () {
            self.page += 1;
            return self.load();
        },
        'load': function () {
            self.isLoading = true;
            var deferred = $q.defer();
            var params = {
                page: self.page,
                per_page: 5
            };

            $http.get('http://dev2.cdm.me/wp-json/wp/v2/posts?_embed', {params: params})
                .success(function (data) {
                    self.isLoading = false;
                    if (data.length == 0) {
                        self.hasMore = false;
                    }
                    angular.forEach(data, function(post) {
                        console.log(data);
                        $http.get('http://dev2.cdm.me/wp-json/wp/v2/users/'+ post.author +'?_embed', {params: params})
                            .success(function (data) {
                                console.log(data);
                                deferred.resolve();
                            })
                            .error(function (data) {
                                deferred.reject(data);
                            });
                        self.results.push(post);
                    });
                    deferred.resolve();
                })
                .error(function (data) {
                    self.isLoading = false;
                    deferred.reject(data);
                });

            return deferred.promise;
        }
    };

    self.load();

    return self;
}