angular
    .module('app')
    .controller('TopTenCtrl', TopTenCtrl);

function TopTenCtrl($scope, $state, TrendingService, PopularService, HotService, $rootScope, $ionicLoading, $cordovaSocialSharing, $timeout) {
    $scope.active_content = 'first';
    $scope.perPage = 10;
    $scope.page = 1;
    $scope.posts = [];
    $rootScope.rootLoader = false;
    $scope.isLoading = true;

    $scope.goToFirst = function () {
        $scope.posts = [];
        $scope.isLoading = true;
        TrendingService.getPosts($scope.page, $scope.perPage)
            .then(function(data) {
                $scope.isLoading = false;
                console.log(data);
                $timeout(function() {
                    $scope.posts = data;
                }, 2700);
            })
            .catch(function(error) {
                console.log(error);
                $scope.isLoading = false;
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
        $scope.active_content = 'first';
    };

    $scope.goToSecond = function () {
        $scope.posts = [];
        $scope.isLoading = true;
        HotService.getPosts($scope.page, $scope.perPage)
            .then(function(data) {
                console.log(data);
                $scope.isLoading = false;
                $timeout(function() {
                    $scope.posts = data;
                }, 2700);
            })
            .catch(function(error) {
                console.log(error);
                $scope.isLoading = false;
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
        $scope.active_content = 'second';
    };

    $scope.goToThree = function () {
        $scope.posts = [];
        $scope.isLoading = true;
        PopularService.getPosts($scope.page, $scope.perPage)
            .then(function(data) {
                $scope.isLoading = false;
                console.log(data);
                $timeout(function() {
                    $scope.posts = data;
                }, 2700);
            })
            .catch(function(error) {
                console.log(error);
                $scope.isLoading = false;
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
            });
        $scope.active_content = 'three';
    };

    $scope.goSingle = function(post) {
        $state.go('singlePost', {post: post, comments: false});
    };

    $scope.doRefresh = function () {
        switch ($scope.active_content) {
            case 'first':
                $scope.goToFirst();
                $scope.$broadcast('scroll.refreshComplete');
                break;
            case 'second':
                console.log("sdsdsd");
                $scope.goToSecond();
                $scope.$broadcast('scroll.refreshComplete');
                break;
            case 'three':
                $scope.goToThree();
                $scope.$broadcast('scroll.refreshComplete');
                break;
            default:
        }
    };

    $scope.goToFirst();

    $scope.goSingle = function(post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: false});
    };

    $scope.goSingleComments = function (post) {
        $rootScope.rootLoader = true;
        $state.go('singlePost', {post: post, comments: true});
    };

    $scope.fbShare = function(link) {
        $cordovaSocialSharing
            .shareViaFacebook(null, null, link)
            .then(function(result) {
                console.log(result);
            })
            .catch(function(err) {
                console.log(err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Facebook aplikacija', noBackdrop: true, duration: 2000 });
            });
    };

    $scope.twitterShare = function(link) {
        $cordovaSocialSharing
            .shareViaTwitter(null, null, link)
            .then(function(result) {
                console.log("Then", result);
            })
            .catch(function(err) {
                console.log("Error", err);
                $ionicLoading.show({ template: 'Na vašem uređaju nije pronađena Twitter aplikacija', noBackdrop: true, duration: 2000 });
            });
    };
}