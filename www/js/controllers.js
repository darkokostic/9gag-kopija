angular
    .module('app')
    .controller('AppCtrl', AppCtrl);

function AppCtrl($scope, $state, AppService, AuthService, $localStorage, $ionicSideMenuDelegate, $ionicHistory, $rootScope, $ionicLoading, $ionicPopup, IonicClosePopupService, $ionicPopover) {
    $ionicPopover.fromTemplateUrl('./templates/user-popover/user-popover.html', {
        scope: $scope
    }).then(function(popover) {
        $rootScope.popover = popover;
    });
    AppService.getCategories()
        .then(function(data) {
            $localStorage.categories = data;
            $scope.categories = data;
        })
        .catch(function(error) {
            console.log(error);
            $scope.categories = $localStorage.categories;
            $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
        });

    $scope.search = function(query) {
        AppService.search(query)
            .then(function(data) {
                if($ionicHistory.currentStateName() == "app.search") {
                    $rootScope.searchPosts = data;
                    $rootScope.query = query;
                    $ionicSideMenuDelegate.toggleLeft(false);
                } else {
                    $rootScope.searchPosts = data;
                    $rootScope.query = query;
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    $state.go('app.search', {data: data, query: query});
                    $ionicSideMenuDelegate.toggleLeft(false);
                }
            })
            .catch(function(error) {
                $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
                if($ionicHistory.currentStateName() == "app.search") {
                    $rootScope.searchPosts = [];
                    $rootScope.query = query;
                    $ionicSideMenuDelegate.toggleLeft(false);
                } else {
                    $rootScope.searchPosts = [];
                    $rootScope.query = query;
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    $state.go('app.search', {data: [], query: query});
                    $ionicSideMenuDelegate.toggleLeft(false);
                }
            });
    };

    $rootScope.showAuthPopup = function() {
        $rootScope.credentials = {
            username: null,
            password: null
        };
        $rootScope.authPopup = $ionicPopup.show({
            templateUrl: "./templates/auth-popup/auth-popup.html",
            scope: $scope
        });
        IonicClosePopupService.register($rootScope.authPopup);
    };

    $scope.login = function(credentials) {
        if(credentials.username && credentials.password) {
            AuthService.login(credentials)
                .then(function(data) {
                    $localStorage.token = data.token;
                    $localStorage.user = data.user;
                    $rootScope.authPopup.close();
                    $ionicLoading.show({ template: "Uspešno ste se ulogovali", noBackdrop: true, duration: 2000 });
                    $ionicSideMenuDelegate.toggleLeft(false);
                })
                .catch(function(error) {
                    $ionicLoading.show({ template: error.message, noBackdrop: true, duration: 2000 });
                });
        } else {
            $ionicLoading.show({ template: "Unesite korisničko ime i šifru", noBackdrop: true, duration: 2000 });
        }
    };

    $scope.logout = function() {
        AuthService.logout();
        $ionicLoading.show({ template: "Uspešno ste se odjavili", noBackdrop: true, duration: 2000 });
        $rootScope.popover.hide();
        $ionicSideMenuDelegate.toggleLeft(false);
    };

    $scope.goCreatePost = function() {
        $state.go('app.newPost');
        $ionicSideMenuDelegate.toggleLeft(false);
    };
}
