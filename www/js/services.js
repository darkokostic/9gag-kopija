angular
    .module('app')
    .service('AppService', AppService);

function AppService($q, $http) {
    function getCategories() {
        var deferred = $q.defer();
        var params = {
            per_page: 30,
            exclude: 1,
            hide_empty: true
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/categories', {params: params})
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });

        return deferred.promise;
    }

    function search(query) {
        var deferred = $q.defer();
        var params = {
            per_page: 10,
            page: 1,
            search: query
        };
        $http.get('http://dev2.cdm.me/wp-json/wp/v2/posts?_embed', {params: params})
            .success(function (data) {
                angular.forEach(data, function(post) {
                    var commParams = {
                        post: post.id,
                        per_page: 1,
                        page: 1
                    };
                    $http.get('http://dev2.cdm.me/wp-json/wp/v2/comments', {params: commParams})
                        .success(function (comments, status, headers) {
                            post.points = 0;
                            post.comments = [];
                            post.comments_count = headers('X-WP-Total');
                        })
                        .error(function (error) {
                            deferred.reject(error);
                        });
                });
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });

        return deferred.promise;
    }

    return {
        getCategories: getCategories,
        search: search
    }
}

