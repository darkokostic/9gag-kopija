angular.module('app', ['ionic', 'ngCordova', 'angularMoment', 'ngStorage', 'ionic.closePopup'])

.run(function ($ionicPlatform, amMoment, $localStorage, $rootScope) {
    if($localStorage.token) {
        $rootScope.isLoggedIn = true;
    } else {
        $rootScope.isLoggedIn = false;
    }
    $rootScope.rootLoader = true;
    amMoment.changeLocale('me');
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.config(function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.interceptors.push('httpInterceptor');

    $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.home', {
        url: '/home',
        abstract: true,
        views: {
            'menuContent': {
                templateUrl: 'templates/home/tabs/home-tabs.html',
                controller: 'HomeTabsCtrl'
            }
        }
    })

    .state('app.home.latest', {
        url: '/latest',
        cache: false,
        views: {
            'tab-latest': {
                templateUrl: 'templates/home/tabs/tab-latest/tab-latest.html',
                controller: 'LatestCtrl'
            }
        }
    })

    .state('app.home.popular', {
        url: '/popular',
        cache: false,
        views: {
            'tab-popular': {
                templateUrl: 'templates/home/tabs/tab-popular/tab-popular.html',
                controller: 'PopularCtrl'
            }
        }
    })

    .state('app.home.hot', {
        url: '/hot',
        cache: false,
        views: {
            'tab-hot': {
                templateUrl: 'templates/home/tabs/tab-hot/tab-hot.html',
                controller: 'HotCtrl'
            }
        }
    })

    .state('app.home.trending', {
        url: '/trending',
        cache: false,
        views: {
            'tab-trending': {
                templateUrl: 'templates/home/tabs/tab-trending/tab-trending.html',
                controller: 'TrendingCtrl'
            }
        }
    })

    .state('singlePost', {
        url: '/post',
        cache: false,
        params: {
            post: null,
            comments: null
        },
        templateUrl: 'templates/single-post/single-post.html',
        controller: 'SinglePostCtrl'
    })

    .state('app.category', {
        url: '/category/:id',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/category/category.html',
                controller: 'CategoryCtrl'
            }
        }
    })

    .state('app.search', {
        url: '/search',
        cache: false,
        params: {
            data: null,
            query: null
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/search/search.view.html',
                controller: 'SearchCtrl'
            }
        }
    })

    .state('app.profile', {
        url: '/profile',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/profile/profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })

    .state('app.newPost', {
        url: '/newpost',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/new-post/new-post.html',
                controller: 'NewPostCtrl'
            }
        }
    })

    .state('app.top-ten', {
        url: '/top-ten',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/top-ten/top-ten.html',
                controller: 'TopTenCtrl'
            }
        }
    });

    $urlRouterProvider.otherwise('/app/home/latest');
});