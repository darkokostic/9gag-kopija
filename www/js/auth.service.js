angular
    .module('app')
    .service('AuthService', AuthService);

function AuthService($q, $http, $localStorage, $rootScope) {

    function login(credentials) {
        var deferred = $q.defer();
        $http.post('http://dev2.cdm.me/wp-json/jwt-auth/v1/token', {username: credentials.username, password: credentials.password})
            .success(function (data) {
                $rootScope.isLoggedIn = true;
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });

        return deferred.promise;
    }

    function logout() {
        delete $localStorage.token;
        delete $localStorage.user;
        $rootScope.isLoggedIn = false;
    }

    return {
        login: login,
        logout: logout
    }
}