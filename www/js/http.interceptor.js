angular
    .module('app')
    .factory('httpInterceptor', httpInterceptor);

function httpInterceptor($q, $localStorage) {
    function request(config) {
        if(config.method == "POST" && config.url != "http://dev2.cdm.me/wp-json/jwt-auth/v1/token" && $localStorage.token) {
            config.headers.Authorization = 'Bearer ' + $localStorage.token;
        }
        return config;
    }

    function response(response) {
        return response;
    }

    function responseError(error) {
        switch (error.status) {
            case 400:
                console.log(error.data.message);
                break;
            case 401:
                delete $localStorage.token;
                delete $localStorage.user;
                $rootScope.isLoggedIn = false;
                break;
            case 403:
                console.log(error.data.message);
                break;
            case 404:
                console.log(error.data.message);
                break;
            case 500:
                console.log(error.data.message);
                break;
            case -1:
                error.data = {
                    data: null,
                    status: -1,
                    message: "Nema internet konekcije"
                };
                break;
            case 0:
                error.data = {
                    data: null,
                    status: 0,
                    message: "Nema internet konekcije"
                };
                break;
        }
        console.log(error);
        return $q.reject(error);
    }

    return {
        request: request,
        response: response,
        responseError: responseError
    };
}