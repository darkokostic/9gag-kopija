angular
    .module('app')
    .directive('scrollableTab', scrollableTab);

function scrollableTab ($compile) {
    function link($scope, element, attrs) {
        $(element).find(".tab-nav.tabs a").wrapAll("<div class='allLinks'></div>");

        var myScroll = $compile("<ion-scroll class='myScroll' dir='ltr' zooming='true' direction='x' style='width: 100%; height: 50px'></ion-scroll>")($scope);

        $(element).find('.allLinks').append(myScroll);
        $(element).find(myScroll).find('.scroll').append($('.allLinks a'));
        $(element).find(myScroll).find("a")
            .wrapAll("<div class='links' style='min-width: 100%'></div>");

    }

    return {
        restrict: 'A',
        link:link
    }
};